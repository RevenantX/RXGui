#include <iostream>
#include <list>
#include <SFML/Graphics.hpp>
#include <cstdio>
#include "guimanager.h"

#include "guibutton.h"
#include "guilabel.h"
#include "guislider.h"
#include "guicheckbox.h"
#include "guiprogressbar.h"

using namespace std;
using namespace rxgui;

GUIManager *manager;
GUIButton *button1, *button2;
GUILabel *label1;
GUISlider *slider1;
GUICheckBox *checkbox1;
GUIProgressBar *progressbar;

void Clicked(const MouseEvent &e)
{
    button2->SetVisible(!button2->IsVisible());
}

void Clicked2(const MouseEvent &e)
{
    slider1->SetValue( (rand()%1000)/10.f );
}

void Changed(int value)
{
    char str[20];
    sprintf(str, "%d", value);
    label1->SetText(str);
    progressbar->SetValue(value);
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");

    TextureAtlas atlas;
    atlas.LoadAtlas("button.xml");

    sf::Font font;
    font.loadFromFile("/usr/share/fonts/TTF/LiberationSans-Regular.ttf");

    manager = new GUIManager(&window);

    button1 = new GUIButton();
    button1->LoadFromAtlas(&atlas);
    button1->SetPosition(10, 20);
    button1->SetSize(128, 32);
    button1->SetFont(font);
    button1->SetCaption("Hide buttona");
    button1->SetCharacterSize(15);
    button1->OnMouseClickEvent.Connect(Clicked);

    button2 = new GUIButton();
    button2->LoadFromAtlas(&atlas);
    button2->SetPosition(350, 20);
    button2->SetSize(250, 32);
    button2->SetFont(font);
    button2->SetCaption("Random value");
    button2->SetCharacterSize(15);
    button2->OnMouseClickEvent.Connect(Clicked2);

    label1 = new GUILabel();
    label1->SetFont(font);
    label1->SetPosition(0, 100);
    label1->SetText("Hello world!");

    slider1 = new GUISlider();
    slider1->SetPosition(0, 300);
    slider1->SetSize(200,16);
    slider1->LoadFromAtlas(&atlas);
    slider1->OnValueChangedEvent.Connect(Changed);

    checkbox1 = new GUICheckBox();
    checkbox1->SetPosition(200,100);
    checkbox1->SetSize(32,32);
    checkbox1->LoadFromAtlas(&atlas);

    progressbar = new GUIProgressBar();
    progressbar->SetPosition(300,300);
    progressbar->SetSize(200,32);
    progressbar->LoadFromAtlas(&atlas);

    manager->AddWidget(progressbar);
    manager->AddWidget(checkbox1);
    manager->AddWidget(button1);
    manager->AddWidget(button2);
    manager->AddWidget(label1);
    manager->AddWidget(slider1);

    while (window.isOpen())
    {
         // Process events
         sf::Event event;
         while (window.pollEvent(event))
         {
             manager->UpdateEvents(event);
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 window.close();
         }

         // Clear screen
         window.clear();

         manager->Render();

         // Update the window
         window.display();
     }

     return 0;
}

