#ifndef GUICHECKBOX_H
#define GUICHECKBOX_H

#include "guiwidget.h"
#include "textureatlas.h"

namespace rxgui
{
class GUICheckBox : public GUIWidget
{
    enum CheckBoxState{ CHECKBOX_CHECKED, CHECKBOX_UNCHECKED, CHECKBOX_CHECKED_DOWN };
public:
    GUICheckBox();
    void LoadFromAtlas(TextureAtlas *atlas);

    virtual void Render(sf::RenderWindow *window);
    virtual void OnMouseClick(const MouseEvent &e);
    void OnPositionChanged(int x, int y);
    void OnSizeChanged(int width, int height);

    void Check();
    void Uncheck();
    bool IsChecked();
private:
    bool mChecked;

    sf::RectangleShape mShape[3];
};
}

#endif // GUICHECKBOX_H
