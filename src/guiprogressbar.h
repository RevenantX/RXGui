#ifndef GUIPROGRESSBAR_H
#define GUIPROGRESSBAR_H

#include "guiwidget.h"
#include "textureatlas.h"

namespace rxgui
{
class GUIProgressBar : public GUIWidget
{
    enum ProgressBarSides { SIDE_LEFT, SIDE_MIDDLE, SIDE_RIGHT };
public:
    GUIProgressBar();
    void LoadFromAtlas(TextureAtlas *atlas);
    void Render(sf::RenderWindow *window);
    void SetValue(int value);
    void SetMaxValue(int value);

    void OnPositionChanged(int x, int y);
    void OnSizeChanged(int width, int height);
private:
    int mValue;
    int mMaxValue;

    sf::RectangleShape mBackShape[3];
    sf::RectangleShape mProgressShape;
};
}

#endif // GUIPROGRESSBAR_H
