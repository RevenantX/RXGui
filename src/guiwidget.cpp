#include "guiwidget.h"

using namespace rxgui;

GUIWidget::GUIWidget() :
    mVisible(true),
    mIsMouseOver(false),
    mIsMouseDown(false)
{
    OnMouseDownEvent .Connect(this, &GUIWidget::OnMouseDown);
    OnMouseMoveEvent .Connect(this, &GUIWidget::OnMouseMove);
    OnMouseClickEvent.Connect(this, &GUIWidget::OnMouseClick);
    OnMouseOverEvent .Connect(this, &GUIWidget::OnMouseOver);
    OnMouseOutEvent  .Connect(this, &GUIWidget::OnMouseOut);

    OnSizeChangedEvent.Connect(this, &GUIWidget::OnSizeChanged);
    OnPositionChangedEvent.Connect(this, &GUIWidget::OnPositionChanged);
}

GUIWidget::~GUIWidget()
{

}

void GUIWidget::UpdateSize()
{
    OnSizeChangedEvent(mRect.width, mRect.height);
}

void GUIWidget::UpdatePosition()
{
    OnPositionChangedEvent(mRect.left, mRect.top);
}

void GUIWidget::SetPosition(int x, int y)
{
    mRect.left = x;
    mRect.top  = y;
    OnPositionChangedEvent(x,y);
}

void GUIWidget::SetSize(int width, int height)
{
    mRect.width  = width;
    mRect.height = height;
    OnSizeChangedEvent(width,height);
}

void GUIWidget::SetPosition(const sf::Vector2i &position)
{
    SetPosition(position.x, position.y);
}

void GUIWidget::SetSize(const sf::Vector2i &size)
{
    SetSize(size.x, size.y);
}

void GUIWidget::SetVisible(bool visible)
{
    mVisible = visible;
}

bool GUIWidget::IsVisible()
{
    return mVisible;
}

bool GUIWidget::IsMouseOver()
{
    return mIsMouseOver;
}

bool GUIWidget::IsMouseDown()
{
    return mIsMouseDown;
}
