#ifndef GUILABEL_H
#define GUILABEL_H

#include "guiwidget.h"

namespace rxgui
{
class GUILabel : public GUIWidget
{
public:
    GUILabel();
    void OnPositionChanged(int x, int y);
    void SetFont(const sf::Font &font);
    void SetCharacterSize(float size);
    void SetText(const sf::String &text);
    void SetText(const char *text);
    virtual void Render(sf::RenderWindow *window);

private:
    sf::Text mText;
};
}

#endif // GUILABEL_H
