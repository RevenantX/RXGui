#ifndef GUIWIDGET_H
#define GUIWIDGET_H

#include "guitypes.h"

namespace rxgui
{
class GUIWidget
{
public:
    GUIWidget();
    virtual ~GUIWidget();
    virtual void Render(sf::RenderWindow *window) {}
    //Events
    virtual void OnMouseDown (const MouseEvent &e) {}
    virtual void OnMouseMove (const MouseEvent &e) {}
    virtual void OnMouseOver (const MouseEvent &e) {}
    virtual void OnMouseOut  (const MouseEvent &e) {}
    virtual void OnMouseClick(const MouseEvent &e) {}
    virtual void OnSizeChanged(int width, int height) {}
    virtual void OnPositionChanged(int x, int y) {}

    void UpdateSize();
    void UpdatePosition();

    void SetPosition(int x, int y);
    void SetSize(int width, int height);
    //SFML helpers
    void SetPosition(const sf::Vector2i &position);
    void SetSize(const sf::Vector2i &size);
    void SetVisible(bool visible);
    bool IsVisible();
    bool IsMouseOver();
    bool IsMouseDown();

    Delegate<const MouseEvent&> OnMouseDownEvent;
    Delegate<const MouseEvent&> OnMouseOverEvent;
    Delegate<const MouseEvent&> OnMouseOutEvent;
    Delegate<const MouseEvent&> OnMouseClickEvent;
    Delegate<const MouseEvent&> OnMouseMoveEvent;
    Delegate<int, int> OnSizeChangedEvent;
    Delegate<int, int> OnPositionChangedEvent;


protected:
    sf::IntRect mRect;

private:
    bool mVisible;
    bool mIsMouseDown;
    bool mIsMouseOver;

    friend class GUIManager;
};
}
#endif // GUIWIDGET_H
