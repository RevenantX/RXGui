#include "guislider.h"

using namespace rxgui;

GUISlider::GUISlider() :
    mMin(0.f),
    mMax(100.f),
    mValue(50.f)
{
    OnValueChangedEvent.Connect(this, &GUISlider::OnValueChanged);
}

void GUISlider::OnPositionChanged(int x, int y)
{
    OnValueChanged(mValue);

    mTrack[TRACK_LEFT  ].setPosition(x, y);
    mTrack[TRACK_MIDDLE].setPosition(x + getShapeWidth(mTrack[TRACK_LEFT]), y);
    mTrack[TRACK_RIGHT ].setPosition(x + mRect.width - getShapeWidth(mTrack[TRACK_RIGHT]), y);
}

void GUISlider::OnSizeChanged(int width, int height)
{
    for(int i = 0; i < 2; i++)
        mThumb[i].setSize(sf::Vector2f(getShapeWidth(mThumb[i]), height));

    mTrack[TRACK_LEFT  ].setSize(sf::Vector2f(getShapeWidth(mTrack[TRACK_LEFT ]), height));
    mTrack[TRACK_RIGHT ].setSize(sf::Vector2f(getShapeWidth(mTrack[TRACK_RIGHT]), height));
    mTrack[TRACK_MIDDLE].setSize(sf::Vector2f(width - getShapeWidth(mTrack[TRACK_LEFT]) - getShapeWidth(mTrack[TRACK_RIGHT]), height));

    UpdatePosition();
}

void GUISlider::Render(sf::RenderWindow *window)
{
    if(IsMouseOver() || IsMouseDown())
        mState = SLIDER_OVER;
    else
        mState = SLIDER_UP;

    window->draw(mTrack[TRACK_LEFT]);
    window->draw(mTrack[TRACK_MIDDLE]);
    window->draw(mTrack[TRACK_RIGHT]);

    window->draw(mThumb[mState]);
}

void GUISlider::SetValue(float value)
{
    if(value > mMax)
        value = mMax;
    else if(value < mMin)
        value = mMin;

    mValue = value;

    OnValueChangedEvent(mValue);
}

const float GUISlider::GetValue()
{
    return mValue;
}

void GUISlider::SetMinimum(float min)
{
    mMin = min;
}

void GUISlider::SetMaximum(float max)
{
    mMax = max;
}

void GUISlider::OnMouseDown(const MouseEvent &e)
{
    SetValue( ( (e.x - mRect.left) * mMax ) / mRect.width );
}

void GUISlider::OnValueChanged(int value)
{
    for(int i = 0; i < 2; i++)
        mThumb[i].setPosition((value * mRect.width) / mMax - getShapeWidth(mThumb[i])*0.5f,mRect.top);
}

void GUISlider::OnMouseMove(const MouseEvent &e)
{
    if(IsMouseDown())
        SetValue( ( (e.x - mRect.left) * mMax ) / mRect.width );
}

void GUISlider::LoadFromAtlas(TextureAtlas *atlas)
{
    TextureGroup *thumbUp    = atlas->GetTextureGroup("slider-thumb-up");
    TextureGroup *thumbOver  = atlas->GetTextureGroup("slider-thumb-over");
    TextureGroup *track      = atlas->GetTextureGroup("slider-track");

    for(int i = 0; i < 2; i++)
        mThumb[i].setTexture(atlas->GetTexture());
    for(int i = 0; i < 3; i++)
        mTrack[i].setTexture(atlas->GetTexture());

    mThumb[SLIDER_UP  ].setTextureRect( thumbUp->GetTextureRect("thumb") );
    mThumb[SLIDER_OVER].setTextureRect( thumbOver->GetTextureRect("thumb") );

    mTrack[TRACK_LEFT  ].setTextureRect( track->GetTextureRect("left") );
    mTrack[TRACK_MIDDLE].setTextureRect( track->GetTextureRect("middle") );
    mTrack[TRACK_RIGHT ].setTextureRect( track->GetTextureRect("right") );

    UpdateSize();
}
