#include "guiprogressbar.h"

using namespace rxgui;

GUIProgressBar::GUIProgressBar() :
    mValue(0),
    mMaxValue(100)
{
}

void GUIProgressBar::LoadFromAtlas(TextureAtlas *atlas)
{
    TextureGroup *progressbar = atlas->GetTextureGroup("progressbar");

    for(int i = 0; i < 3; i++)
    {
        mBackShape[i].setTexture(atlas->GetTexture());
    }
    mProgressShape.setTexture(atlas->GetTexture());

    mBackShape[SIDE_LEFT  ].setTextureRect(progressbar->GetTextureRect("left"));
    mBackShape[SIDE_MIDDLE].setTextureRect(progressbar->GetTextureRect("middle"));
    mBackShape[SIDE_RIGHT ].setTextureRect(progressbar->GetTextureRect("right"));
    mProgressShape.setTextureRect(progressbar->GetTextureRect("progress"));

    UpdateSize();
}

void GUIProgressBar::SetValue(int value)
{
    if(value <= mMaxValue)
        mValue = value;
    else
        mValue = mMaxValue;

    mProgressShape.setSize(sf::Vector2f(mValue * mRect.width / mMaxValue, mRect.height));
}

void GUIProgressBar::SetMaxValue(int value)
{
    if(value > 0)
        mMaxValue = value;
    SetValue(mValue);
}

void GUIProgressBar::Render(sf::RenderWindow *window)
{
    for(int i = 0; i < 3; i++)
        window->draw(mBackShape[i]);

    window->draw(mProgressShape);
}

void GUIProgressBar::OnPositionChanged(int x, int y)
{
    mBackShape[SIDE_LEFT  ].setPosition(x, y);
    mBackShape[SIDE_MIDDLE].setPosition(x + getShapeWidth( mBackShape[SIDE_LEFT] ), y);
    mBackShape[SIDE_RIGHT ].setPosition(x + mRect.width - getShapeWidth( mBackShape[SIDE_RIGHT] ), y);

    mProgressShape.setPosition(x, y);
}

void GUIProgressBar::OnSizeChanged(int width, int height)
{
    mBackShape[SIDE_LEFT  ].setSize(sf::Vector2f(getShapeWidth(mBackShape[SIDE_LEFT]), height));
    mBackShape[SIDE_RIGHT ].setSize(sf::Vector2f(getShapeWidth(mBackShape[SIDE_RIGHT]), height));
    mBackShape[SIDE_MIDDLE].setSize(sf::Vector2f(width - getShapeWidth(mBackShape[SIDE_LEFT]) - getShapeWidth(mBackShape[SIDE_RIGHT]), height));
    UpdatePosition();
}
