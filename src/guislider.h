#ifndef GUISLIDER_H
#define GUISLIDER_H

#include "guiwidget.h"
#include "textureatlas.h"

namespace rxgui
{
enum SliderStates { SLIDER_UP, SLIDER_OVER };
enum TrackSides { TRACK_LEFT, TRACK_MIDDLE, TRACK_RIGHT };

class GUISlider : public GUIWidget
{
public:
    GUISlider();
    void OnPositionChanged(int x, int y);
    void OnSizeChanged(int width, int height);
    virtual void OnValueChanged(int value);

    virtual void Render(sf::RenderWindow *window);

    void OnMouseDown(const MouseEvent &e);
    void OnMouseMove(const MouseEvent &e);

    void SetValue(float value);
    const float GetValue();
    void SetMinimum(float min);
    void SetMaximum(float max);

    void LoadFromAtlas(TextureAtlas *atlas);

    Delegate<float> OnValueChangedEvent;
private:
    float mValue;
    float mStep;
    float mMin, mMax;
    int mState;
    sf::RectangleShape mTrack[3];
    sf::RectangleShape mThumb[2];
};
}

#endif // GUISLIDER_H
