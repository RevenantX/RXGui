#ifndef TEXTUREATLAS_H
#define TEXTUREATLAS_H

#include <SFML/Graphics.hpp>
#include <map>

namespace rxgui
{
inline int getShapeWidth(const sf::RectangleShape &shape)
{
    return shape.getTextureRect().width;
}

class TextureGroup
{
    typedef std::map<std::string, sf::IntRect> TextureInfoContainer;

public:
    //TextureGroup();

    sf::IntRect GetTextureRect(const char *name);
private:
    friend class TextureAtlas;

    TextureInfoContainer mInfos;
    sf::IntRect mNullRect;
};

class TextureAtlas
{
    typedef std::map<std::string, TextureGroup*> GroupContainer;

public:
    TextureAtlas();
    sf::Texture *GetTexture();
    void LoadAtlas(const char *fileName);

    TextureGroup *GetTextureGroup(const char *name);
private:
    sf::Texture mAtlasTexture;
    GroupContainer mGroups;
};
}

#endif // TEXTUREATLAS_H
