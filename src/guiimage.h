#ifndef GUIIMAGE_H
#define GUIIMAGE_H

#include "guiwidget.h"

namespace rxgui
{
class GUIImage : public GUIWidget
{
public:
    GUIImage();
    void OnPositionChanged(int x, int y);
    void SetTexture(const sf::Texture *texture);
    void OnSizeChanged(int width, int height);
    void Render(sf::RenderWindow *window);

private:
    sf::RectangleShape mShape;
};
}

#endif // GUIIMAGE_H
