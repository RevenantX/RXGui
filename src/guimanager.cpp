#include "guimanager.h"

using namespace rxgui;

GUIManager::GUIManager(sf::RenderWindow *renderWindow) :
    mRenderWindow(renderWindow),
    mDownedWidget(nullptr)
{

}

void GUIManager::Render()
{
    std::list<GUIWidget*>::iterator it = mWidgets.begin();
    while(it != mWidgets.end())
    {
        GUIWidget *w = (*it);
        if(w->mVisible)
            w->Render(mRenderWindow);

        it++;
    }
}

void GUIManager::UpdateEvents(const sf::Event &e)
{
    sf::Vector2i mousePosition = sf::Mouse::getPosition((*mRenderWindow));
    MouseEvent mouseEvent;
    mouseEvent.x = mousePosition.x;
    mouseEvent.y = mousePosition.y;

    std::list<GUIWidget*>::iterator it = mWidgets.begin();
    for(it; it != mWidgets.end(); it++)
    {
        GUIWidget *w = (*it);

        if(!w->mVisible)
            continue;

        bool mouseOver = false;

        //MouseOver check
        if(w->mRect.contains(mousePosition.x, mousePosition.y))
        {
            mouseOver = true;
            if(!w->mIsMouseOver)
            {
                w->OnMouseOverEvent(mouseEvent);
                w->mIsMouseOver = true;
            }
        }
        else
        {
            if(w->mIsMouseOver)
            {
                w->OnMouseOutEvent(mouseEvent);
                w->mIsMouseOver = false;
            }
        }

        //Clicks Check
        if(mouseOver)
        {
            if(e.type == sf::Event::MouseMoved)
            {
                w->OnMouseMoveEvent(mouseEvent);
            }
            if(e.type == sf::Event::MouseButtonPressed)
            {
                mDownedWidget = w;

                mouseEvent.mouseButton = e.mouseButton.button;
                w->OnMouseDownEvent(mouseEvent);
                w->mIsMouseDown = true;
            }
            if(e.type == sf::Event::MouseButtonReleased && w == mDownedWidget)
            {
                mouseEvent.mouseButton = e.mouseButton.button;
                w->OnMouseClickEvent(mouseEvent);
            }
        }
    }

    //Global check
    if(mDownedWidget)
    {
        if(e.type == sf::Event::MouseMoved)
        {
            mDownedWidget->OnMouseMoveEvent(mouseEvent);
        }
        if(e.type == sf::Event::MouseButtonReleased)
        {
            mDownedWidget->mIsMouseDown = false;
            mDownedWidget = nullptr;
        }
    }
}

void GUIManager::AddWidget(GUIWidget *widget)
{
    mWidgets.push_back(widget);
}
