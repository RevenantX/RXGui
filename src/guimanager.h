#ifndef GUIMANAGER_H
#define GUIMANAGER_H

#include "guiwidget.h"
#include <list>

namespace rxgui
{
class GUIManager
{
public:
    GUIManager(sf::RenderWindow *renderWindow);

    void Render();
    void UpdateEvents(const sf::Event &e);
    void AddWidget(GUIWidget *widget);

private:
    std::list<GUIWidget*> mWidgets;
    sf::RenderWindow *mRenderWindow;
    GUIWidget *mDownedWidget;
};
}

#endif // GUIMANAGER_H
