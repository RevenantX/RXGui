#ifndef GUITYPES_H
#define GUITYPES_H

#include <SFML/Graphics.hpp>
#include "Delegate.h"

namespace rxgui
{
typedef struct
{
    int x;
    int y;
    int mouseButton;
} MouseEvent;
}

#endif // GUITYPES_H
