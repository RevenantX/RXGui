#include "guilabel.h"

using namespace rxgui;

GUILabel::GUILabel()
{
}

void GUILabel::OnPositionChanged(int x, int y)
{
    mText.setPosition(x, y);
}

void GUILabel::SetFont(const sf::Font &font)
{
    mText.setFont(font);
}

void GUILabel::SetCharacterSize(float size)
{
    mText.setCharacterSize(size);
}

void GUILabel::SetText(const sf::String &text)
{
    mText.setString(text);
}

void GUILabel::SetText(const char *text)
{
    mText.setString(text);
}

void GUILabel::Render(sf::RenderWindow *window)
{
    window->draw(mText);
}
