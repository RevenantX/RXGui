#include "guiimage.h"

using namespace rxgui;

GUIImage::GUIImage()
{
}

void GUIImage::OnPositionChanged(int x, int y)
{
    mShape.setPosition(x, y);
}

void GUIImage::OnSizeChanged(int width, int height)
{
    mShape.setSize(sf::Vector2f(width, height));
}

void GUIImage::SetTexture(const sf::Texture *texture)
{
    mShape.setTexture(texture);
}

void GUIImage::Render(sf::RenderWindow *window)
{
    window->draw(mShape);
}
