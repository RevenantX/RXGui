#include "textureatlas.h"
#include "pugixml/pugixml.hpp"
#include <iostream>

using namespace std;
using namespace rxgui;

sf::IntRect TextureGroup::GetTextureRect(const char *name)
{
    TextureInfoContainer::iterator it = mInfos.find(name);
    if(it == mInfos.end())
    {
        cerr << "Error: can't find: " << name << " texture info" << endl;
        return mNullRect;
    }

    return it->second;
}

TextureAtlas::TextureAtlas()
{
}

void TextureAtlas::LoadAtlas(const char *fileName)
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(fileName);
    if(!result)
    {
        cerr << "Error loading: " << fileName << endl;
        return;
    }

    //Loading image
    pugi::xml_node texture = doc.child("texture");
    mAtlasTexture.loadFromFile(texture.attribute("file").value());

    //Loading groups
    for (pugi::xml_node group = texture.child("group"); group; group = group.next_sibling("group"))
    {
        const char *groupName = group.attribute("name").value();
        mGroups[groupName] = new TextureGroup();

        //Creating parts
        for (pugi::xml_node part = group.child("part"); part; part = part.next_sibling("part"))
        {
            sf::IntRect rect;
            const char *partName = part.attribute("name").value();

            rect.left   = part.attribute("pos_x").as_int();
            rect.top    = part.attribute("pos_y").as_int();
            rect.width  = part.attribute("width").as_int();
            rect.height = part.attribute("height").as_int();

            mGroups[groupName]->mInfos[partName] = rect;
        }
    }

}

sf::Texture *TextureAtlas::GetTexture()
{
    return &mAtlasTexture;
}

TextureGroup *TextureAtlas::GetTextureGroup(const char *name)
{
    GroupContainer::iterator it = mGroups.find(name);

    if(it == mGroups.end())
    {
        cerr << "Error: can't find " << name << " texture group" << endl;
        return nullptr;
    }

    return it->second;
}
