#include "guicheckbox.h"

using namespace rxgui;

GUICheckBox::GUICheckBox() :
    mChecked(false)
{

}

void GUICheckBox::LoadFromAtlas(TextureAtlas *atlas)
{
    TextureGroup *checkBox = atlas->GetTextureGroup("checkbox");

    for(int i = 0; i < 3; i++)
    {
        mShape[i].setTexture(atlas->GetTexture());
    }

    mShape[CHECKBOX_UNCHECKED   ].setTextureRect(checkBox->GetTextureRect("unchecked"));
    mShape[CHECKBOX_CHECKED     ].setTextureRect(checkBox->GetTextureRect("checked"));
    mShape[CHECKBOX_CHECKED_DOWN].setTextureRect(checkBox->GetTextureRect("checked-down"));
}

void GUICheckBox::Render(sf::RenderWindow *window)
{
    int state;

    if(mChecked)
    {
        if(IsMouseDown())
            state = CHECKBOX_CHECKED_DOWN;
        else
            state = CHECKBOX_CHECKED;
    }
    else
        state = CHECKBOX_UNCHECKED;

    window->draw(mShape[state]);
}

void GUICheckBox::OnMouseClick(const MouseEvent &e)
{
    mChecked = !mChecked;
}

void GUICheckBox::OnPositionChanged(int x, int y)
{
    for(int i = 0; i < 3; i++)
    {
        mShape[i].setPosition(x, y);
    }
}

void GUICheckBox::OnSizeChanged(int width, int height)
{
    for(int i = 0; i < 3; i++)
    {
        mShape[i].setSize( sf::Vector2f(width, height) );
    }
}

void GUICheckBox::Check()
{
    mChecked = true;
}

void GUICheckBox::Uncheck()
{
    mChecked = false;
}

bool GUICheckBox::IsChecked()
{
    return mChecked;
}
