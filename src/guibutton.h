#ifndef GUIBUTTON_H
#define GUIBUTTON_H

#include "guiwidget.h"
#include "textureatlas.h"

namespace rxgui
{
class GUIButton : public GUIWidget
{
    enum ButtonStates { BUTTON_UP, BUTTON_DOWN, BUTTON_OVER };
public:
    GUIButton();
    void OnPositionChanged(int x, int y);
    void OnSizeChanged(int width, int height);
    void LoadFromAtlas(TextureAtlas *atlas);

    void SetFont(const sf::Font &font);
    void SetCaption(const char *caption);
    void SetCharacterSize(unsigned int size);

    virtual void Render(sf::RenderWindow *window);
private:
    void UpdateTextPosition();

    sf::RectangleShape mLeftShape[3];
    sf::RectangleShape mRightShape[3];
    sf::RectangleShape mCenterShape[3];

    sf::Text mText;
    int mState;
};
}

#endif // GUIBUTTON_H
