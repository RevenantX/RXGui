#include "guibutton.h"

using namespace rxgui;

GUIButton::GUIButton()
{

}

void GUIButton::OnPositionChanged(int x, int y)
{
    for(int i = 0; i < 3; i++)
    {
        mLeftShape  [i].setPosition(x, y);
        mCenterShape[i].setPosition(x + getShapeWidth(mLeftShape[i]), y);
        mRightShape [i].setPosition(x + mRect.width - getShapeWidth(mRightShape[i]), y);
    }
    UpdateTextPosition();
}

void GUIButton::OnSizeChanged(int width, int height)
{
    for(int i = 0; i < 3; i++)
    {
        mLeftShape [i].setSize(sf::Vector2f(getShapeWidth(mLeftShape [i]), height));
        mRightShape[i].setSize(sf::Vector2f(getShapeWidth(mRightShape[i]), height));
        mCenterShape[i].setSize(sf::Vector2f(width - getShapeWidth(mLeftShape[i]) - getShapeWidth(mRightShape[i]), height));
    }
    UpdatePosition();
}

void GUIButton::LoadFromAtlas(TextureAtlas *atlas)
{
    TextureGroup *buttonUp   = atlas->GetTextureGroup("button-up");
    TextureGroup *buttonDown = atlas->GetTextureGroup("button-down");
    TextureGroup *buttonOver = atlas->GetTextureGroup("button-over");

    for(int i = 0; i < 3; i++)
    {
        mLeftShape  [i].setTexture(atlas->GetTexture());
        mCenterShape[i].setTexture(atlas->GetTexture());
        mRightShape [i].setTexture(atlas->GetTexture());
    }

    mLeftShape[BUTTON_UP  ].setTextureRect(buttonUp  ->GetTextureRect("left"));
    mLeftShape[BUTTON_DOWN].setTextureRect(buttonDown->GetTextureRect("left"));
    mLeftShape[BUTTON_OVER].setTextureRect(buttonOver->GetTextureRect("left"));

    mCenterShape[BUTTON_UP  ].setTextureRect(buttonUp  ->GetTextureRect("middle"));
    mCenterShape[BUTTON_DOWN].setTextureRect(buttonDown->GetTextureRect("middle"));
    mCenterShape[BUTTON_OVER].setTextureRect(buttonOver->GetTextureRect("middle"));

    mRightShape[BUTTON_UP  ].setTextureRect(buttonUp  ->GetTextureRect("right"));
    mRightShape[BUTTON_DOWN].setTextureRect(buttonDown->GetTextureRect("right"));
    mRightShape[BUTTON_OVER].setTextureRect(buttonOver->GetTextureRect("right"));
}

void GUIButton::SetFont(const sf::Font &font)
{
    mText.setFont(font);
    UpdateTextPosition();
}

void GUIButton::SetCaption(const char *caption)
{
    mText.setString(caption);
    UpdateTextPosition();
}

void GUIButton::SetCharacterSize(unsigned int size)
{
    mText.setCharacterSize(size);
    UpdateTextPosition();
}

void GUIButton::Render(sf::RenderWindow *window)
{
    if(IsMouseDown())
        mState = BUTTON_DOWN;
    else if(IsMouseOver())
        mState = BUTTON_OVER;
    else
        mState = BUTTON_UP;

    window->draw(mLeftShape[mState]);
    window->draw(mCenterShape[mState]);
    window->draw(mRightShape[mState]);
    window->draw(mText);
}

void GUIButton::UpdateTextPosition()
{
    const sf::FloatRect &rect = mText.getLocalBounds();

    int x = mRect.left + mRect.width /2.f - rect.width /2.f;
    int y = mRect.top  + mRect.height/2.f - rect.height/2.f - 3.f; //SOME MAGIC!!!

    mText.setPosition(x, y);
}
